<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_3c4a71774cf48014341eeb410871d733b84c26ec06b31484c809cfe9f7ea64ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c829fec19a8ef2ddec3618091622fe612b2d3de81de33569e17666a0de1e61b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c829fec19a8ef2ddec3618091622fe612b2d3de81de33569e17666a0de1e61b->enter($__internal_4c829fec19a8ef2ddec3618091622fe612b2d3de81de33569e17666a0de1e61b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_dfbce4fe64fddadfc248441858d2ecdfefcd6702c4a63eff6fedb05bee5b335e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfbce4fe64fddadfc248441858d2ecdfefcd6702c4a63eff6fedb05bee5b335e->enter($__internal_dfbce4fe64fddadfc248441858d2ecdfefcd6702c4a63eff6fedb05bee5b335e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_4c829fec19a8ef2ddec3618091622fe612b2d3de81de33569e17666a0de1e61b->leave($__internal_4c829fec19a8ef2ddec3618091622fe612b2d3de81de33569e17666a0de1e61b_prof);

        
        $__internal_dfbce4fe64fddadfc248441858d2ecdfefcd6702c4a63eff6fedb05bee5b335e->leave($__internal_dfbce4fe64fddadfc248441858d2ecdfefcd6702c4a63eff6fedb05bee5b335e_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\icon-minus-square.svg");
    }
}
