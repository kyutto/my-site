<?php

/* @WebProfiler/Icon/time.svg */
class __TwigTemplate_1f838ed4b93f9c0c98ab8676c42d249107dba96d78bfc0243cdb488d55e9a204 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d88c22291d78806e55e094366da177d5e05cc694ee200bdeb4425db6dd8803c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d88c22291d78806e55e094366da177d5e05cc694ee200bdeb4425db6dd8803c->enter($__internal_1d88c22291d78806e55e094366da177d5e05cc694ee200bdeb4425db6dd8803c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/time.svg"));

        $__internal_263415a509f8a27df565f1f41633b74526f9b4f9c0bbe2c3d23a468324fcd033 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_263415a509f8a27df565f1f41633b74526f9b4f9c0bbe2c3d23a468324fcd033->enter($__internal_263415a509f8a27df565f1f41633b74526f9b4f9c0bbe2c3d23a468324fcd033_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/time.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M15.1,4.3c-2.1-0.5-4.2-0.5-6.2,0C8.6,4.3,8.2,4.1,8.2,3.8V3.4c0-1.2,1-2.3,2.3-2.3h3c1.2,0,2.3,1,2.3,2.3
    v0.3C15.8,4.1,15.4,4.3,15.1,4.3z M20.9,14c0,4.9-4,8.9-8.9,8.9s-8.9-4-8.9-8.9s4-8.9,8.9-8.9S20.9,9.1,20.9,14z M16.7,15
    c0-0.6-0.4-1-1-1H13V8.4c0-0.6-0.4-1-1-1s-1,0.4-1,1v6.2c0,0.6,0.4,1.3,1,1.3h3.7C16.2,16,16.7,15.6,16.7,15z\"/>
</svg>
";
        
        $__internal_1d88c22291d78806e55e094366da177d5e05cc694ee200bdeb4425db6dd8803c->leave($__internal_1d88c22291d78806e55e094366da177d5e05cc694ee200bdeb4425db6dd8803c_prof);

        
        $__internal_263415a509f8a27df565f1f41633b74526f9b4f9c0bbe2c3d23a468324fcd033->leave($__internal_263415a509f8a27df565f1f41633b74526f9b4f9c0bbe2c3d23a468324fcd033_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/time.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M15.1,4.3c-2.1-0.5-4.2-0.5-6.2,0C8.6,4.3,8.2,4.1,8.2,3.8V3.4c0-1.2,1-2.3,2.3-2.3h3c1.2,0,2.3,1,2.3,2.3
    v0.3C15.8,4.1,15.4,4.3,15.1,4.3z M20.9,14c0,4.9-4,8.9-8.9,8.9s-8.9-4-8.9-8.9s4-8.9,8.9-8.9S20.9,9.1,20.9,14z M16.7,15
    c0-0.6-0.4-1-1-1H13V8.4c0-0.6-0.4-1-1-1s-1,0.4-1,1v6.2c0,0.6,0.4,1.3,1,1.3h3.7C16.2,16,16.7,15.6,16.7,15z\"/>
</svg>
", "@WebProfiler/Icon/time.svg", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Icon\\time.svg");
    }
}
