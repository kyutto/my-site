<?php

/* PimcoreCoreBundle:Profiler:logo.svg.twig */
class __TwigTemplate_6ab53fb075b37d53883770d26aec071a09e8fedf2ab1bd6330b396fb18f6670f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7c17284afcb2a5f541fbe1c27f6d7730bc67dde9abc6162874ca6fb16d5a0c8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c17284afcb2a5f541fbe1c27f6d7730bc67dde9abc6162874ca6fb16d5a0c8c->enter($__internal_7c17284afcb2a5f541fbe1c27f6d7730bc67dde9abc6162874ca6fb16d5a0c8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:logo.svg.twig"));

        $__internal_c5b3a472fe18dffecb11133f4ba7c2bfd97d4f2aa78589913d38027e31b90076 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5b3a472fe18dffecb11133f4ba7c2bfd97d4f2aa78589913d38027e31b90076->enter($__internal_c5b3a472fe18dffecb11133f4ba7c2bfd97d4f2aa78589913d38027e31b90076_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:logo.svg.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"utf-8\"?>
<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 45 26.25\">
    <path d=\"M38.13,16.63a9.37,9.37,0,0,0-7.82,4.21L23.44,31.18a9.38,9.38,0,1,1,0-10.42l1.23,1.86,2.25-3.4-.33-.5a13.12,13.12,0,1,0,0,14.53l2.55-3.85,1.18,1.78a9.38,9.38,0,1,0,7.82-14.55Zm0,15a5.62,5.62,0,0,1-4.7-2.54l-2-3.09,2.06-3.11a5.62,5.62,0,1,1,4.69,8.73Z\" transform=\"translate(-2.5 -12.88)\" style=\"fill:#fff\"/>
</svg>
";
        
        $__internal_7c17284afcb2a5f541fbe1c27f6d7730bc67dde9abc6162874ca6fb16d5a0c8c->leave($__internal_7c17284afcb2a5f541fbe1c27f6d7730bc67dde9abc6162874ca6fb16d5a0c8c_prof);

        
        $__internal_c5b3a472fe18dffecb11133f4ba7c2bfd97d4f2aa78589913d38027e31b90076->leave($__internal_c5b3a472fe18dffecb11133f4ba7c2bfd97d4f2aa78589913d38027e31b90076_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:logo.svg.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"utf-8\"?>
<svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 45 26.25\">
    <path d=\"M38.13,16.63a9.37,9.37,0,0,0-7.82,4.21L23.44,31.18a9.38,9.38,0,1,1,0-10.42l1.23,1.86,2.25-3.4-.33-.5a13.12,13.12,0,1,0,0,14.53l2.55-3.85,1.18,1.78a9.38,9.38,0,1,0,7.82-14.55Zm0,15a5.62,5.62,0,0,1-4.7-2.54l-2-3.09,2.06-3.11a5.62,5.62,0,1,1,4.69,8.73Z\" transform=\"translate(-2.5 -12.88)\" style=\"fill:#fff\"/>
</svg>
", "PimcoreCoreBundle:Profiler:logo.svg.twig", "D:\\PROJECT\\SIMPLY PROJECT\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/logo.svg.twig");
    }
}
